//
//  naviViewController.swift
//  newapp
//
//  Created by systimanx on 16/03/21.
//

import UIKit

class naviViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        let navLabel = UILabel()
        let navTitle = NSMutableAttributedString(string: "PLAIN", attributes:[
                                                    NSAttributedString.Key.foregroundColor: UIColor.blue,
                                                    NSAttributedString.Key.font: UIFont.systemFont(ofSize: 17.0, weight: UIFont.Weight.light)])

        navTitle.append(NSMutableAttributedString(string: "BOLD", attributes:[
                                                    NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 17.0),
                                                    NSAttributedString.Key.foregroundColor: UIColor.blue]))

        navLabel.attributedText = navTitle
        self.navigationItem.titleView = navLabel
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
