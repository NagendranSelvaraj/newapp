//
//  newViewController.swift
//  newapp
//
//  Created by systimanx on 16/03/21.
//

import UIKit

class newViewController: UIViewController {

    @IBOutlet var home: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        home.text = "Hello"
        home.textColor = UIColor.white
        home.backgroundColor = UIColor.brown
        home.font = .italicSystemFont(ofSize: 30)
        home.textAlignment = .center
        home.shadowColor = UIColor.black
        home.isHighlighted = true
        
        

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
