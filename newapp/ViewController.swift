import UIKit
  
class ViewController: UIViewController {
    
    /* @IBOutlet weak var textLabel: UILabel!   // Label
     @IBOutlet weak var textInput: UITextField!  // Text
     @IBAction func onButtonTop(sender: UIButton){ }// Button
     @IBOutlet weak var datepicker: UIDatePicker! // Date picker
     @IBOutlet weak var slider: UISlider! // Slider
     @IBOutlet weak var stepper: UIStepper! // Stepper
     @IBOutlet weak var mySwitch: UISwitch! // Switch
     @IBOutlet weak var segmentedControl: UISegmentedControl! // SegmentedControl */
    
    
     
    private let button: UIButton = {
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: 200, height: 52))
        button.setTitle("Log in", for: .normal)
        button.backgroundColor = .white
        button.setTitleColor(.black, for: .normal)
        return button
    }()
    
   
      
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        view.backgroundColor = .systemBlue
        view.addSubview(button)
        button.addTarget(self, action: #selector(didTopButton), for: .touchUpInside)
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        button.center = view.center
    }
   
    @objc func didTopButton(){
        let tapBarVC = UITabBarController()
        
        let vc1 = UINavigationController(rootViewController: FirstViewController())
        let vc2 = UINavigationController(rootViewController: SecondViewController())
        let vc3 = UINavigationController(rootViewController: ThirdViewController())
        let vc4 = UINavigationController(rootViewController: FourthViewController())
        let vc5 = UINavigationController(rootViewController: FifthViewController())
        vc1.title = "Home"
        vc2.title = "Contact"
        vc3.title = "Help"
        vc4.title = "About"
        vc5.title = "Settings"
        tapBarVC.setViewControllers([vc1,vc2,vc3,vc4,vc5], animated: false)
        
        guard let items = tapBarVC.tabBar.items else {
            return
        }
        let images = ["house","bell","person.circle","star","gear"]
        for x in 0..<items.count{
            items[x].badgeValue = "2"
            items[x].image = UIImage(systemName: images[x])
        }
       
        tapBarVC.modalPresentationStyle = .fullScreen
        present(tapBarVC, animated:true)
    }
    
    class FirstViewController: UIViewController {
        override func viewDidLoad() {
            super.viewDidLoad()
            view.backgroundColor = .red
            title = "Home"
        }
    }
    class SecondViewController: UIViewController {
        override func viewDidLoad() {
            super.viewDidLoad()
            view.backgroundColor = .green
            title = "Contact"
        }
    }
    class ThirdViewController: UIViewController {
        override func viewDidLoad() {
            super.viewDidLoad()
            view.backgroundColor = .blue
            title = "Help"
        }
    }
    class FourthViewController: UIViewController {
        override func viewDidLoad() {
            super.viewDidLoad()
            view.backgroundColor = .gray
            title = "About"
        }
    }
    class FifthViewController: UIViewController {
        override func viewDidLoad() {
            super.viewDidLoad()
            view.backgroundColor = .yellow
            title = "Settings"
        }
    }
}
